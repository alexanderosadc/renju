﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int n;
    public bool playerTurn;
    public GameObject[,] guiTable;
    public char[,] table;
    public string winPlayer;
    public GameObject currentCell;
    public int bestRow;
    public int bestColumn;
    public char aiBot;
	public char human;
    public enum GameStates {StartMenu, Gameplay, EndMenu};
    public GameStates currentGameState;
    public Color defaultColor;
    private void Awake() {
        currentGameState = GameStates.StartMenu;
    }
    void Start()
    {
        guiTable = new GameObject[n, n];
        table = new char[n, n];
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                table[i, j] = '1';
            }
        }
        
        if(!instance)
        {
            instance = this;
        }
    }
    public bool EndGameConditionAI()
    {
        if(LineCheckAI() || ColumnCheckAI() || DiagonalCheckAI() || SecondDiagonalCheckAI())
        {
            return true;
        }
        return false;
    }
    public bool LineCheckAI()
    {
        int posI = GameManager.instance.bestRow;
        int posJ = GameManager.instance.bestColumn;
        for(int j = 0; j < GameManager.instance.n; j++)
        {
            if(GameManager.instance.table[posI, j] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[posI, j] == '1')
            {
                return false;
            }
        }
        return true;
    }
    public bool ColumnCheckAI()
    {
        int posI = GameManager.instance.bestRow;
        int posJ = GameManager.instance.bestColumn;
        for(int i = 0; i < GameManager.instance.n; i++)
        {
            if(GameManager.instance.table[i, posJ] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[i, posJ] == '1')
            {
                return false;
            }
        }
        return true;
    }
    public bool DiagonalCheckAI()
    {
        int posI = GameManager.instance.bestRow;
        int posJ = GameManager.instance.bestColumn;
        

        for(int i = 0; i < GameManager.instance.n; i++)
        {
            int j = i;
            if(GameManager.instance.table[i, j] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[i, j] == '1')
            {
                return false;
            }
        }
        return true;
    }

    public bool SecondDiagonalCheckAI()
    {
        int posI = GameManager.instance.bestRow;
        int posJ = GameManager.instance.bestColumn;
        for(int i = GameManager.instance.n - 1; i >= 0; i--)
        {
            int j = GameManager.instance.n - i - 1;

            if(GameManager.instance.table[i, j] != GameManager.instance.table[posJ, posI] || 
                GameManager.instance.table[i, j] == '1')
            {
                return false;
            }
        }
        return true;
    }

    public bool EndGameConditionPlayer()
    {
        Debug.Log(LineCheck() + " " + ColumnCheck()+ " " + DiagonalCheck()+ " " + SecondDiagonalCheck());
        if(LineCheck() || ColumnCheck() || DiagonalCheck() || SecondDiagonalCheck())
        {
            return true;
        }
        return false;
    }

    public bool LineCheck()
    {
        int posI = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexI;
        int posJ = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexJ;
        for(int j = 0; j < GameManager.instance.n; j++)
        {
            if(GameManager.instance.table[posI, j] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[posI, j] == '1')
            {
                return false;
            }
        }
        return true;
    }

    public bool ColumnCheck()
    {
        int posI = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexI;
        int posJ = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexJ;
        for(int i = 0; i < GameManager.instance.n; i++)
        {
            if(GameManager.instance.table[i, posJ] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[i, posJ] == '1')
            {
                return false;
            }
        }
        return true;
    }
    public bool DiagonalCheck()
    {
        int posI = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexI;
        int posJ = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexJ;
        

        for(int i = 0; i < GameManager.instance.n; i++)
        {
            int j = i;
            if(GameManager.instance.table[i, j] != GameManager.instance.table[posI, posJ] || 
                GameManager.instance.table[i, j] == '1')
            {
                return false;
            }
        }
        return true;
    }
    public bool SecondDiagonalCheck()
    {
        int posI = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexI;
        int posJ = GameManager.instance.currentCell.GetComponent<InputPlayer>().indexJ;

        for(int i = GameManager.instance.n - 1; i >= 0; i--)
        {
            int j = GameManager.instance.n - i - 1;
            Debug.Log(GameManager.instance.table[i, j] + " " + GameManager.instance.table[posI, posJ]);
            if(GameManager.instance.table[i, j] != GameManager.instance.table[posJ, posI] || 
                GameManager.instance.table[i, j] == '1')
            {
                Debug.Log(GameManager.instance.table[posI, posJ]);
                return false;
            }
        }
        return true;
    }
}