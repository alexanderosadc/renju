﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryForNodes : MonoBehaviour {
	public Vector3 startPositionForNodes;
	public GameObject pref;
	public Vector3 distanceBetweenNodes;
	private void Start() {
		StartCoroutine(InstantiateNodes());
	}

	IEnumerator InstantiateNodes()
	{
		yield return new WaitForSeconds(.1f);
		Vector3 position = startPositionForNodes;
		for(int i = 0; i < GameManager.instance.n; i++)
		{
			position = new Vector3(startPositionForNodes.x, position.y, position.z);
			for(int j = 0; j < GameManager.instance.n; j++)
			{
				GameObject nod = Instantiate(pref, position, Quaternion.identity);
				nod.transform.parent = gameObject.transform;
				nod.transform.GetChild(0).GetComponent<SpriteRenderer>().color = GameManager.instance.defaultColor;
				//nod.GetComponent<SpriteRenderer>().enabled = false;
				GameManager.instance.guiTable[i, j] = nod;
				position += new Vector3(distanceBetweenNodes.x, 0f, 0f);
			}
			position -= new Vector3(0f, distanceBetweenNodes.y, 0f);
		}
		yield return null;
	}
}