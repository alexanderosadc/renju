﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiBot : MonoBehaviour {
	
	bool iterator = false;
	private void Start() 
	{
		StartCoroutine(Delayed(.0001f));
	}
	IEnumerator Delayed(float time)
	{
		while(true)
		{
			if(iterator)
			{
				yield return new WaitForSeconds(time);
				findBestMove();
				// Debug.Log(Evaluate(GameManager.instance.table));

			}
			else
			{
				yield return new WaitForSeconds(.0001f);
			}
		}
	}
	
	private void Update() {
		if(!GameManager.instance.playerTurn && GameManager.instance.currentGameState == GameManager.GameStates.Gameplay)
		{

			
			// findBestMove();
			iterator = true;
			
		}
	}
	bool isMovesLeft(char[,] table) 
	{ 
		for (int i = 0; i<GameManager.instance.n; i++) 
			for (int j = 0; j<GameManager.instance.n; j++) 
				if (table[i, j]=='1') 
					return true; 
		return false; 
	} 
	int Evaluate(char[,] table)
	{
		Debug.Log("Ahtung");
		Debug.Log(GameManager.instance.n);
		for(int i = 2; i < GameManager.instance.n - 2; i++)
		{
			for(int j = 2; j < GameManager.instance.n - 2; j++)
			{
					if (table[i, j - 2] == table[i, j - 1] && 
						table[i, j - 1] == table[i, j]  && 
						table[i, j]     == table[i, j + 1]  && 
						table[i, j + 1] == table[i, j + 2]) 
					{ 
						if (table[i, j] == GameManager.instance.aiBot) 
							return +10; 
						else if (table[i, j] == GameManager.instance.human) 
							return -10;
					}
					if (table[i - 2, j] == table[i - 1, j] && 
						table[i - 1, j] == table[i, j]  && 
						table[i, j]     == table[i + 1, j]  && 
						table[i + 1, j] == table[i + 2, j]) 
					{ 
						if (table[i, j ] == GameManager.instance.aiBot) 
							return +10; 
				
						else if (table[i, j] == GameManager.instance.human) 
							return -10; 
					}
					if (table[i - 2, j - 2]==table[i - 1, j - 1] && table[i - 1, j - 1]==table[i, j] && 
					table[i, j]==table[i + 1, j] && table[i + 1, j + 1]==table[i + 2, j + 2]) 
					{ 
						if (table[i, j]==GameManager.instance.aiBot) 
							return +10; 
						else if (table[i, j]==GameManager.instance.human) 
							return -10; 
					}

					if (table[i - 2, j + 2]==table[i - 1, j + 1] && table[i - 1, j + 1]==table[i, j] && 
						table[i, j]==table[i + 1, j - 1] && table[i + 2, j - 2]==table[i + 1, j - 1]) 
					{
						if (table[0, 2]==GameManager.instance.aiBot) 
							return +10; 
						else if (table[0, 2]==GameManager.instance.human) 
							return -10; 
					}
			}
		}
		return 0; 
	}

	int MiniMax(char[,] table, int depth, bool isMax, int alpha, int beta)
	{
		int score = Evaluate(table); 
  
		if (score == 10) 
			return score; 
	
		if (score == -10) 
			return score; 
	
		if (isMovesLeft(table)==false) 
			return 0; 
	
		if (isMax) 
		{ 
			int best = -1000; 
	
			for (int i = 0; i<GameManager.instance.n; i++) 
			{ 
				for (int j = 0; j<GameManager.instance.n; j++) 
				{ 
					if (table[i, j]=='1') 
					{ 
						table[i, j] = GameManager.instance.aiBot; 
						best = Mathf.Max(best, 
							MiniMax(table, depth + 1, !isMax, alpha, beta));
						alpha = Mathf.Max(alpha, best);
						table[i, j] = '1';
						if(beta <= alpha)
						{
							break;
							break;
						}
					}
				}
			}
			return best; 
		}
		else
		{ 
			int best = 1000; 
			for (int i = 0; i<GameManager.instance.n; i++) 
			{ 
				for (int j = 0; j<GameManager.instance.n; j++) 
				{ 
					if (table[i, j]=='1') 
					{ 
						table[i, j] = GameManager.instance.human; 
						best = Mathf.Min(best, 
							MiniMax(table, depth+1, !isMax, alpha, beta)); 
						beta = Mathf.Min(beta, best);
						table[i, j] = '1'; 

						if(beta <= alpha)
						{
							break;
							break;
						}
					} 
				} 
			} 
			return best; 
		}  
	}

	void findBestMove() 
	{
		char[,] table = GameManager.instance.table;
		int bestVal = -1000; 
		int bestCol = -1;
		int bestRow = -1;

		Debug.Log("lalala");
		for (int i = 0; i < GameManager.instance.n; i++) 
		{
			for (int j = 0; j < GameManager.instance.n; j++) 
			{
				if (table[i, j]=='1') 
				{ 
					table[i, j] = GameManager.instance.aiBot; 
	
					int moveVal = MiniMax(table, 0, false, 1000, -1000); 
	
					table[i, j] = '1';
	
					if (moveVal > bestVal) 
					{ 
						bestRow = i; 
						bestCol = j; 
						bestVal = moveVal;
					} 
				} 
			}
		}
		if(bestCol == -1 || bestRow == -1)
		{
			UIRef.instance.Equall();
		}
		else
		{
			GameManager.instance.bestRow = bestRow;
			GameManager.instance.bestColumn = bestCol;
			GameManager.instance.table[bestRow , bestCol] = GameManager.instance.aiBot;
			UIRef.instance.PutTableInGUI(bestRow, bestCol);
			
			if(GameManager.instance.EndGameConditionAI())
			{
				UIRef.instance.AIWins();
			}
			GameManager.instance.playerTurn = true;
			UIRef.instance.RefreshUI();
		}
		if(!CheckForFreeSpaces())
		{
			UIRef.instance.Equall();
		}
		iterator = false;
	}

	private bool CheckForFreeSpaces()
	{
		int counter = 0;

		for(int i = 0; i < GameManager.instance.n; i++)
		{
			for(int j = 0; j < GameManager.instance.n; j++)
			{
				if(GameManager.instance.table[i, j] == '1')
				{
					counter++;
				}
			}
		}
		if(counter == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}