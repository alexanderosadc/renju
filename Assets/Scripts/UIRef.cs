﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRef : MonoBehaviour
{
    public static UIRef instance;
    public Text turnState;
    public GameObject startPanel;
    public GameObject finalPanel;
    public Text finalText;
    void Start()
    {
        if(!instance)
        {
            instance = this;
        }
    }

    public void RefreshUI()
    {
        if(GameManager.instance.playerTurn)
        {
            turnState.text = "Your Turn";
            turnState.color = Color.green;
        }
        else
        {
            turnState.text = "AI Turn";
            turnState.color = Color.red;
        }
    }

    public void PutDataInTable()
    {
        int i = 0;
        int j = 0;
        foreach (var item in GameManager.instance.guiTable)
        {
            if(item.transform.GetChild(0).GetComponent<SpriteRenderer>().color == GameManager.instance.defaultColor)
            {
                GameManager.instance.table[i, j] = ' ';
            }
            if(j == GameManager.instance.n - 1)
            {
                j = 0;
                i++;
            }
            else
            {
                j++;
            }
        }
    }

    public void PutTableInGUI(int row, int column)
    {
        for(int i = 0; i <= GameManager.instance.n; i++)
        {
            for(int j = 0; j <= GameManager.instance.n; j++)
            {
                if(i == row && j == column)
                {
                    Color col = new Color();
                    if(GameManager.instance.table[i, j] == '0')
                    {
                        col = Color.white;
                    }
                    else if(GameManager.instance.table[i, j] == 'x')
                    {
                        col = Color.black;
                    }
                    GameManager.instance.guiTable[i, j].transform.GetChild(0).GetComponent<SpriteRenderer>().color = col;
                    GameManager.instance.guiTable[i, j].transform.GetComponent<InputPlayer>().activeObject = false;
                }
            }
        }
            
    }

    public void PlayerWins()
    {
        finalText.text = "Humanity Wins!";
        finalText.color = Color.green;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }

    public void AIWins()
    {
        finalText.text = "AI Wins!";
        finalText.color = Color.red;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }

    public void Equall()
    {
        finalText.text = "Humanity still fighting!!!";
        finalText.color = Color.grey;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }
}