﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPlayer : MonoBehaviour
{
    public bool activeObject;
    public int indexI;
    public int indexJ;
    private void Start() 
    {
        activeObject = true;
    }

    private void OnMouseDown() 
    {
        if(activeObject)
        {
            if(GameManager.instance.playerTurn && GameManager.instance.currentGameState == GameManager.GameStates.Gameplay)
            {
                if(GameManager.instance.human == '0')
                {
                    gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.white;
                }
                else if(GameManager.instance.human == 'x')
                {
                    gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.black;
                }
                gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                GameManager.instance.playerTurn = false;
                GameManager.instance.currentCell = gameObject;
                activeObject = false;
                // UIRef.instance.PutDataInTable();
                UIRef.instance.RefreshUI();
                if(GameManager.instance.EndGameConditionPlayer())
                {
                    UIRef.instance.PlayerWins();
                }
            }
        }
    }
}